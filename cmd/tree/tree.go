package tree

import (
	"fmt"
)

type node struct {
	v int
	l *node
	r *node
}

type Tree struct {
	root *node
}

func New() *Tree {
	return &Tree{nil}
}

func (t *Tree) Insert(v int) {
	if t.root == nil {
		t.root = &node{v, nil, nil}
		return
	}
	insertNode(t.root, v)
}

func insertNode(curr *node, v int) {
	if v < curr.v {
		if curr.l == nil {
			curr.l = &node{v, nil, nil}
		} else {
			insertNode(curr.l, v)
		}
		return
	}
	if curr.r == nil {
		curr.r = &node{v, nil, nil}
	} else {
		insertNode(curr.r, v)
	}
}

func (t *Tree) Contains(v int) bool {
	return contains(t.root, v)
}

func contains(curr *node, v int) bool {
	if curr == nil {
		return false
	}
	if curr.v == v {
		return true
	}
	if v < curr.v {
		return contains(curr.l, v)
	}
	return contains(curr.r, v)
}

func (t *Tree) Remove(v int) bool {
	nodeToRemove := findNode(v, t.root)
	if nodeToRemove == nil {
		return false
	}
	parent := findParent(v, t.root)
	switch {
	case nodeToRemove == t.root:
		t.root = nil
	case nodeToRemove.l == nil && nodeToRemove.r == nil:
		if nodeToRemove.v < parent.v {
			parent.l = nil
		} else {
			parent.r = nil
		}
	case nodeToRemove.l == nil && nodeToRemove.r != nil:
		if nodeToRemove.v < parent.v {
			parent.l = nodeToRemove.r
		} else {
			parent.r = nodeToRemove.r
		}
	case nodeToRemove.l != nil && nodeToRemove == nil:
		if nodeToRemove.v < parent.v {
			parent.l = nodeToRemove.l
		} else {
			parent.r = nodeToRemove.l
		}
	default:
		largestValue := nodeToRemove.l
		for largestValue.r != nil {
			largestValue = largestValue.r
		}
		findParent(largestValue.v, t.root).r = nil
		nodeToRemove.v = largestValue.v
	}
	return true
}

func findNode(v int, curr *node) *node {
	if curr == nil {
		return nil
	}
	if curr.v == v {
		return curr
	}
	if v < curr.v {
		return findNode(v, curr.l)
	}
	return findNode(v, curr.r)
}

func findParent(v int, curr *node) *node {
	if v == curr.v {
		return nil
	}
	if v < curr.v {
		if curr.l == nil {
			return nil
		}
		if curr.l.v == v {
			return curr
		}
		return findParent(v, curr.l)
	}
	if curr.r == nil {
		return nil
	}
	if curr.r.v == v {
		return curr
	}
	return findParent(v, curr.r)
}

func (t *Tree) Min() int {
	return min(t.root)
}

func min(curr *node) int {
	if curr.l == nil {
		return curr.v
	}
	return min(curr.l)
}

func (t *Tree) Max() int {
	return max(t.root)
}

func max(curr *node) int {
	if curr.r == nil {
		return curr.v
	}
	return max(curr.r)
}

func (t *Tree) Preorder() (s string) {
	return preorder(t.root)
}

func preorder(curr *node) (s string) {
	if curr != nil {
		s += fmt.Sprint(curr.v)
		s += preorder(curr.l)
		s += preorder(curr.r)
	}
	return
}

func (t *Tree) Postorder() (s string) {
	return postorder(t.root)
}

func postorder(curr *node) (s string) {
	if curr != nil {
		s += postorder(curr.l)
		s += postorder(curr.r)
		s += fmt.Sprint(curr.v)
	}
	return
}

func (t *Tree) Inorder() (s string) {
	return inorder(t.root)
}

func inorder(curr *node) (s string) {
	if curr != nil {
		s += inorder(curr.l)
		s += fmt.Sprint(curr.v)
		s += inorder(curr.r)
	}
	return
}
