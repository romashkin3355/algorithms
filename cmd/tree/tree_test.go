package tree

import (
	"testing"
)

func TestTree(t *testing.T) {
	tree := New()
	if t == nil {
		t.Error("Tree is nil")
	}
	c := tree.Contains(3)
	if c {
		t.Error("Tree contains not existing value")
	}
	tree.Insert(1)
	c = tree.Contains(1)
	if !c {
		t.Error("Tree doesn't contain inserted value")
	}
	r := tree.Remove(1)
	if !r {
		t.Error("Item still in the list")
	}
	if tree.Contains(1) {
		t.Error("Item still in the list")
	}
	tree.Insert(2)
	tree.Insert(5)
	tree.Insert(8)
	tree.Insert(3)
	tree.Insert(6)
	tree.Insert(1)
	m := tree.Min()
	if m != 1 {
		t.Errorf("Expected 1, but obtained: %d", m)
	}
	m = tree.Max()
	if m != 8 {
		t.Errorf("Expected 8, but obtained: %d", m)
	}
	p := tree.Preorder()
	if p != "215386" {
		t.Error("Expected 215386, but obtained: " + p)
	}
	p = tree.Postorder()
	if p != "136852" {
		t.Error("Expected 136852, but obtained: " + p)
	}
	p = tree.Inorder()
	if p != "123568" {
		t.Error("Expected 123568, but obtained: " + p)
	}
}
