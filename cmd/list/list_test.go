package list

import (
	"testing"
)

func TestSingly(t *testing.T) {
	testList(NewSingly(), t)
}

func TestDoubly(t *testing.T) {
	testList(NewDoubly(), t)
}

func testList(l List, t *testing.T) {
	if l == nil {
		t.Error("List didn't created")
	}
	r := l.Remove(3)
	if r {
		t.Error("Not existing item has been removed")
	}
	if l.Size() != 0 {
		t.Error("List is not empty")
	}
	s := l.Traverse()
	if s != "" {
		t.Error("Content should be empty, but traverse returns: " + s)
	}
	s = l.ReverseTraverse()
	if s != "" {
		t.Error("Content should be empty, but ReverseTraverse returns : " + s)
	}
	l.Add(5)
	size := l.Size()
	if size != 1 {
		t.Error("Item hasn't been added")
	}
	if !l.Contains(5) {
		t.Error("Item hasn't been added")
	}
	r = l.Remove(5)
	if !r {
		t.Error("Item still in the list")
	}
	if l.Contains(5) {
		t.Error("Item still in the list")
	}
	size = l.Size()
	if size != 0 {
		t.Error("Item still in the list")
	}
	l.Add(1)
	l.Add(2)
	l.Add(3)
	l.Add(4)
	size = l.Size()
	if size != 4 {
		t.Errorf("List size should be equals to 4, current value = %d", size)
	}
	r = l.Remove(3)
	if !r {
		t.Error("Item still in the list")
	}
	if l.Contains(3) {
		t.Error("Item still in the list")
	}
	size = l.Size()
	if size != 3 {
		t.Errorf("List size should be equals to 3, current value = %d", size)
	}
	r = l.Remove(3)
	if r {
		t.Error("Not existing item has been removed")
	}
	s = l.Traverse()
	if s != "1 2 4" {
		t.Errorf("Expected '1 2 4', but obtained: '%s'", s)
	}
	s = l.ReverseTraverse()
	if s != "4 2 1" {
		t.Errorf("Expected '4 2 1', but obtained: '%s'", s)
	}
	r = l.Remove(4)
	if !r {
		t.Error("Item still in the list")
	}
	l.Add(6)
	s = l.Traverse()
	if s != "1 2 6" {
		t.Errorf("Expected '1 2 6', but obtained: '%s'", s)
	}
}
