package list

type List interface {
	Add(v int)
	Remove(v int) bool
	Size() int
	Traverse() string
	ReverseTraverse() string
	Contains(v int) bool
}

type node struct {
	v    int
	next *node
	prev *node
}
