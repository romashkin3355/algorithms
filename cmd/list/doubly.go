package list

import "fmt"

type DoublyLinkedList struct {
	head *node
	tail *node
}

func NewDoubly() *DoublyLinkedList {
	return &DoublyLinkedList{nil, nil}
}

func (l *DoublyLinkedList) Add(v int) {
	n := &node{v, nil, nil}
	if l.head == nil {
		l.head, l.tail = n, n
		return
	}
	if l.tail != nil {
		n.prev, l.tail.next, l.tail = l.tail, n, n
	}
}

func (l *DoublyLinkedList) Size() (size int) {
	for n := l.head; n != nil; n = n.next {
		size++
	}
	return
}

func (l *DoublyLinkedList) Remove(v int) bool {
	if l.head == nil {
		return false
	}
	if v == l.head.v {
		if l.head == l.tail {
			l.head, l.tail = nil, nil
		} else {
			l.head, l.head.prev = l.head.next, nil
		}
		return true
	}
	n := l.head.next
	for n != nil && v != n.v {
		n = n.next
	}
	if n == l.tail {
		l.tail, l.tail.next = l.tail.prev, nil
		return true
	}
	if n != nil {
		n.prev.next, n.next.prev = n.next, n.prev
		return true
	}
	return false
}

func (l *DoublyLinkedList) Contains(v int) bool {
	n := l.head
	for n != nil && n.v != v {
		n = n.next
	}
	return n != nil
}

func (l *DoublyLinkedList) Traverse() (s string) {
	for n := l.head; n != nil; n = n.next {
		s += fmt.Sprint(n.v)
		if n.next != nil {
			s += " "
		}
	}
	return
}

func (l *DoublyLinkedList) ReverseTraverse() (s string) {
	for n := l.tail; n != nil; n = n.prev {
		s += fmt.Sprint(n.v)
		if n.prev != nil {
			s += " "
		}
	}
	return
}
