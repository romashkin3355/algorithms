package list

import (
	"fmt"
)

type SinglyLinkedList struct {
	head *node
	tail *node
}

func NewSingly() *SinglyLinkedList {
	return &SinglyLinkedList{}
}

func (l *SinglyLinkedList) Add(v int) {
	n := &node{v, nil, nil}
	if l.head == nil {
		l.head, l.tail = n, n
		return
	}
	if l.tail != nil {
		l.tail.next, l.tail = n, n
	}
}

func (l *SinglyLinkedList) Contains(v int) bool {
	n := l.head
	for n != nil && n.v != v {
		n = n.next
	}
	return n != nil
}

func (l *SinglyLinkedList) Remove(v int) bool {
	if l.head == nil {
		return false
	}
	n := l.head
	if n != nil && n.v == v {
		if l.head == l.tail {
			l.head, l.tail = nil, nil
		} else {
			l.head = l.head.next
		}
		return true
	}
	for n != nil && n.next != nil && n.next.v != v {
		n = n.next
	}
	if n != nil && n.next != nil {
		if n.next == l.tail {
			l.tail = n
		}
		n.next = n.next.next
		return true
	}
	return false
}

func (l *SinglyLinkedList) Size() (size int) {
	for n := l.head; n != nil; n = n.next {
		size++
	}
	return
}

func (l *SinglyLinkedList) Traverse() (s string) {
	for n := l.head; n != nil; n = n.next {
		s += fmt.Sprint(n.v)
		if n.next != nil {
			s += " "
		}
	}
	return
}

func (l *SinglyLinkedList) ReverseTraverse() (s string) {
	if l.tail != nil {
		curr := l.tail
		for curr != l.head {
			prev := l.head
			for prev.next != curr {
				prev = prev.next
			}
			if curr != l.tail {
				s += " "
			}
			s += fmt.Sprint(curr.v)
			curr = prev
		}
		s += fmt.Sprintf(" %d", curr.v)
	}
	return
}
